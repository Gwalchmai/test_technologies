<?php
/**
 * Created by: Dylan Moss
 * Date: 30/08/2017
 * Description: Staff controller
 */

class staff extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('staff_model');
		$this->load->model('departments_model'); //needed for function get_departments()
	}

	public function index()
	{
		$aData['aStaff'] = $this->staff_model->get_unassigned_staff();

		$this->load->view('templates/header');
		$this->load->view('staff/list', $aData);
		$this->load->view('templates/footer');
	}

	public function deactivate($iStaffID, $sOrigin)
	{
		$this->staff_model->deactivate_person($iStaffID, $sOrigin);
		$sRedirect = base_url() . $sOrigin . '/index';
		header("Location: $sRedirect");
	}

	public function edit($iStaffID)
	{
		$aData['aPerson'] = $this->staff_model->get_staff_member($iStaffID);
		$aData['aDepartments'] = $this->departments_model->get_departments();

		$this->load->view('templates/header');
		$this->load->view('staff/edit', $aData);
		$this->load->view('templates/footer');
	}

	public function new_person()
	{
		$aData['aDepartments'] = $this->departments_model->get_departments();

		$this->load->view('templates/header');
		$this->load->view('staff/create', $aData);
		$this->load->view('templates/footer');
	}

	public function save($iStaffID)
	{
		$this->staff_model->save_person($iStaffID);

		$sRedirect = base_url() . 'staff';
		header("Location: $sRedirect");
	}
}