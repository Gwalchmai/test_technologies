<?php
/**
 * Created by: Dylan Moss
 * Date: 30/08/2017
 * Description: Departments controller
 */

Class Departments extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('departments_model');
	}

	public function index()
	{
		$aData['aDepartments'] = $this->departments_model->get_department_list();

		$this->load->view('templates/header');
		$this->load->view('departments/list', $aData);
		$this->load->view('templates/footer');
	}

	public function deactivate($iDepartmentID)
	{
		$this->departments_model->deactivate_department($iDepartmentID);

		$sRedirect = base_url();
		header("Location: $sRedirect");
	}

	public function edit($iDepartmentID)
	{
		$aData = $this->departments_model->get_department_details($iDepartmentID);

		$this->load->view('templates/header');
		$this->load->view('departments/edit', $aData);
		$this->load->view('templates/footer');
	}

	public function new_department(){
		$aData['aUnassignedStaff'] = $this->departments_model->get_unassigned_staff();

		$this->load->view('templates/header');
		$this->load->view('departments/create', $aData);
		$this->load->view('templates/footer');
	}

	public function save($iDepartmentID = 0)
	{
		$this->departments_model->save_department($iDepartmentID);

		$sRedirect = base_url();
		header("Location: $sRedirect");
	}
}