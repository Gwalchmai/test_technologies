<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//departments routes
$route['departments'] = 'departments/index';
$route['departments/deactivate/(:any)'] = 'departments/deactivate/$1';
$route['departments/edit/(:any)'] = 'departments/edit/$1';
$route['departments/new_department'] = 'departments/new_department';
$route['departments/save/(:any)'] = 'departments/save/$1';

//staff routes
$route['staff'] = 'staff/index';
$route['staff/deactivate/(:any)/(:any)'] = 'staff/deactivate/$1/$2';
$route['staff/edit/(:any)'] = 'staff/edit/$1';
$route['staff/new_person'] = 'staff/new_person';
$route['staff/save/(:any)'] = 'staff/save/$1';

//default routes
$route['default_controller'] = 'departments/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
