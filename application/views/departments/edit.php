<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Edit Department
 */;
$iDepartmentID = filter_var($aDepartment['department_id'], FILTER_SANITIZE_NUMBER_INT);

//set up manager drop-down
$sManagerOptions = '';
foreach($aStaff as $iKey => $aPerson) {
	if($aDepartment['manager_id'] == $aPerson['id']) $sManagerOptions .= "<option value=\"$aPerson[id]\" selected>$aPerson[title] $aPerson[firstname] $aPerson[surname]</option>"; else $sManagerOptions .= "<option value=\"$aPerson[id]\">$aPerson[title] $aPerson[firstname] $aPerson[surname]</option>";
}
$sSubmitURL = base_url() . "departments/save/$iDepartmentID";
?>
<div class="container">
	<div class="form-horizontal">
		<?php echo form_open("$sSubmitURL"); ?>
		<fieldset>
			<legend>Edit Department</legend>

			<div class="form-group">
				<label for="inputClub" class="col-md-2 control-label">Department Name</label>
				<div class="col-md-10">
					<input class="form-control" name="inputDepartmentName" id="inputDepartmentName" placeholder="Department Name" value="<?= $aDepartment['department_name'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputManager" class="col-md-2 control-label">Manager</label>
				<div class="col-md-10">
					<select class="form-control" name="inputManager" id="inputManager"><?= $sManagerOptions ?></select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="reset" class="btn btn-default">Cancel</button>
					<input type="submit" name="Submit" id="submit_btn" class="btn btn-primary">
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>