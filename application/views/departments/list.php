<?php
/**
 * Created by: Dylan Moss
 * Date: 30/08/2017
 * Description: Home page body
 */
?>
<div class="container">
	<legend>Departments</legend>
	<table class="table table-striped table-hover">
		<tr>
			<th>Name</th>
			<th>Manager</th>
			<th></th>
		</tr>
		<?php if(isset($aDepartments) && is_array($aDepartments)) {
			foreach($aDepartments as $iKey => $aDepartment):
				$sManagerName = "$aDepartment[title] $aDepartment[firstname] $aDepartment[surname]";
				?>
				<tr>
					<td><?= ucfirst($aDepartment['department_name']) ?></td>
					<td><?= $sManagerName ?></td>
					<td>
						<button class="btn btn-info btn-sm" data-toggle="collapse" data-target="#department_<?= $aDepartment['department_id'] ?>">Staff</button>
						<a href="<?php echo base_url(); ?>departments/edit/<?= $aDepartment['department_id'] ?>" class="btn btn-info btn-sm">Edit</a>
						<div class="btn btn-danger btn-sm" data-toggle="modal" data-target="#Deactivate">Deactivate</div>
						<div id="Deactivate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Confirm Deactivate Department</h4>
									</div>
									<div class="modal-body">
										<p>Please confirm the department deactivation. All department members will become available for re-assignment after this action.</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary" onclick="window.location.href='departments/deactivate/<?= $aDepartment['department_id'] ?>'">Deactivate Department</button>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
				<td colspan="3">
				<div id="department_<?= $aDepartment['department_id'] ?>" class="collapse">
				<?php if(isset($aDepartment['aStaff']) && is_array($aDepartment['aStaff']) && !empty($aDepartment['aStaff'])) {
				?>
				<table class="table table-striped table-hover">
					<tr>
						<th>Name</th>
						<th>Address</th>
						<th>Contact Details</th>
						<th></th>
					</tr>
					<?php foreach($aDepartment['aStaff'] as $iKey => $aPerson):
						$sNameColumn = "$aPerson[title] $aPerson[firstname] $aPerson[surname]";
						$sAddressColumn = "$aPerson[street_address] <br> $aPerson[suburb] <br> $aPerson[city]";
						$sContactDetailsColumn = "Mobile: $aPerson[mobile_number] <br> Work: $aPerson[work_number] <br> Email: $aPerson[email]";
						?>
						<tr>
							<td><?= $sNameColumn ?></td>
							<td><?= $sAddressColumn ?></td>
							<td><?= $sContactDetailsColumn ?></td>
							<td>
								<a href="<?php echo base_url(); ?>staff/edit/<?= $aPerson['id'] ?>" class="btn btn-info btn-sm">Edit</a>
								<div class="btn btn-danger btn-sm" data-toggle="modal" data-target="#DeactivateStaff">Deactivate</div>
								<div id="DeactivateStaff" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title">Confirm Deactivate Staff Member</h4>
											</div>
											<div class="modal-body">
												<p>Please confirm the staff member deactivation. The staff member will not be available for department allocation after this action.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-sm btn-primary" onclick="window.location.href='<?= base_url() ?>staff/deactivate/<?= $aPerson['id'] ?>/departments'">Deactivate Staff Member</button>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>

			<?php } else { ?>
				<table class="table table-striped table-hover">
					<tr>
						<td colspan="4">No Staff loaded for this Department.</td>
					</tr>
				</table>
			<?php } ?>


			<?php endforeach;
		} else { ?>
			<tr>
				<td colspan="4">No Departments Loaded</td>
			</tr>
		<?php }; ?>
</div>
</td>
</tr>
<tr>
	<td colspan="4" style="text-align: right;"><a href="<?php echo base_url(); ?>departments/new_department" class="btn btn-primary btn-sm">New Department</a></td>
</tr>
</table>
</div>
