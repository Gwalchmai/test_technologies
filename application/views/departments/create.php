<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Create Department
 */;

//set up manager drop-down
$sManagerOptions = '';
foreach ($aUnassignedStaff as $iKey => $aPerson){
	$sManagerOptions .= "<option value=\"$aPerson[id]\">$aPerson[title] $aPerson[firstname] $aPerson[surname]</option>";
}
$sSubmitURL = base_url() . "departments/save/0";
?>
	<div class="container">
	<div class="form-horizontal">
<?php echo form_open("$sSubmitURL"); ?>
	<fieldset>
		<legend>Edit Department</legend>

		<div class="form-group">
			<label for="inputClub" class="col-md-2 control-label">Department Name</label>
			<div class="col-md-10">
				<input class="form-control" name="inputDepartmentName" id="inputDepartmentName" placeholder="Department Name" type="text" required>
			</div>
		</div>

		<div class="form-group">
			<label for="inputManager" class="col-md-2 control-label">Manager</label>
			<div class="col-md-10">
				<select class="form-control" name="inputManager" id="inputManager"><?= $sManagerOptions ?></select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-2">
				<button type="reset" class="btn btn-default">Cancel</button>
				<input type="submit" name="Submit" id="submit_btn" class="btn btn-primary">
			</div>
		</div>
	</fieldset>
	</form>
	</div>
	</div>