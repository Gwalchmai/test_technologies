<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Header view template
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo link_tag('assets/css/styles.css'); ?>
	<title>Staff Management</title>
</head>
<body>
<div>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="pull-left" style="padding: 20px;"><img src="<?= base_url() ?>assets/images/HBS_brand_logo_28.png" height="100px"></div>
			<div class="navbar-brand" style="padding-top: 110px">Staff Management</div>
			<ul class="nav nav-tabs" style="padding-top: 100px">
				<li class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
				<li><a href="<?php echo base_url(); ?>staff">Manage Unassigned Staff</a></li>
				<li><a href="<?php echo base_url(); ?>departments">Manage Departments</a></li>
			</ul>
		</div>
	</nav>
</div>
