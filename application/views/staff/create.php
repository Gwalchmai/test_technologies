<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Create Staff Member
 */;

//set up department drop-down
$sDepartmentOptions = '';
foreach($aDepartments as $iKey => $aDepartment) {
	$sDepartmentName = ucfirst($aDepartment['department_name']);
	$sDepartmentOptions .= "<option value=\"$aDepartment[id]\">$sDepartmentName</option>";
}
$sSubmitURL = base_url() . "staff/save/0";
?>
<div class="container">
	<div class="form-horizontal">
		<?php echo form_open("$sSubmitURL"); ?>
		<fieldset>
			<legend>Create Staff Member</legend>

			<div class="form-group">
				<label for="inputTitle" class="col-md-2 control-label">Title</label>
				<div class="col-md-10">
					<input class="form-control" name="inputTitle" id="inputTitle" placeholder="Title"" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputFirstname" class="col-md-2 control-label">First Name</label>
				<div class="col-md-10">
					<input class="form-control" name="inputFirstname" id="inputFirstname" placeholder="First Name" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputSurname" class="col-md-2 control-label">Surname</label>
				<div class="col-md-10">
					<input class="form-control" name="inputSurname" id="inputSurname" placeholder="Surname" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputStreet" class="col-md-2 control-label">Street Address</label>
				<div class="col-md-10">
					<input class="form-control" name="inputStreet" id="inputStreet" placeholder="Street Address" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputSuburb" class="col-md-2 control-label">Suburb</label>
				<div class="col-md-10">
					<input class="form-control" name="inputSuburb" id="inputSuburb" placeholder="Suburb" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputCity" class="col-md-2 control-label">City</label>
				<div class="col-md-10">
					<input class="form-control" name="inputCity" id="inputCity" placeholder="City" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputMobile" class="col-md-2 control-label">Mobile Number</label>
				<div class="col-md-10">
					<input class="form-control" name="inputMobile" id="inputMobile" placeholder="Mobile Number" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputWork" class="col-md-2 control-label">Work Number</label>
				<div class="col-md-10">
					<input class="form-control" name="inputWork" id="inputWork" placeholder="Work Number" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputEmail" class="col-md-2 control-label">Email Address</label>
				<div class="col-md-10">
					<input class="form-control" name="inputEmail" id="inputEmail" placeholder="Email Address" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputDepartmentID" class="col-md-2 control-label">Department</label>
				<div class="col-md-10">
					<select class="form-control" name="inputDepartmentID" id="inputDepartmentID"><?= $sDepartmentOptions ?></select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="reset" class="btn btn-default">Cancel</button>
					<input type="submit" name="Submit" id="submit_btn" class="btn btn-primary">
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>