<?php
/**
 * Created by: Dylan Moss
 * Date: 30/08/2017
 * Description: Staff page body
 */
?>
<div class="container">
	<legend>Staff Members</legend>
	<table class="table table-striped table-hover">
		<tr>
			<th>Name</th>
			<th>Address</th>
			<th>Contact Details</th>
			<th>Department</th>
			<th></th>
		</tr>
		<?php if(isset($aStaff) && is_array($aStaff)) {
			foreach($aStaff as $iKey => $aPerson):
				$sNameColumn = "$aPerson[title] $aPerson[firstname] $aPerson[surname]";
				$sAddressColumn = "$aPerson[street_address] <br> $aPerson[suburb] <br> $aPerson[city]";
				$sContactDetailsColumn = "Mobile: $aPerson[mobile_number] <br> Work: $aPerson[work_number] <br> Email: $aPerson[email]";
				$sDepartmentColumn = isset($aPerson['department_name']) ? $aPerson['department_name'] : 'Unassigned';
		?>
				<tr>
					<td><?= $sNameColumn ?></td>
					<td><?= $sAddressColumn ?></td>
					<td><?= $sContactDetailsColumn ?></td>
					<td><?= ucfirst($sDepartmentColumn) ?></td>
					<td>
						<a href="<?php echo base_url(); ?>staff/edit/<?= $aPerson['id'] ?>" class="btn btn-info btn-sm">Edit</a>
						<div class="btn btn-danger btn-sm" data-toggle="modal" data-target="#Deactivate">Deactivate</div>
						<div id="Deactivate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Confirm Deactivate Staff Member</h4>
									</div>
									<div class="modal-body">
										<p>Please confirm the staff member deactivation. The staff member will not be available for department allocation after this action.</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary" onclick="window.location.href='<?= base_url() ?>staff/deactivate/<?= $aPerson['id'] ?>/staff'">Deactivate Staff Member</button>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			<?php endforeach;
		} else { ?>
			<tr>
				<td colspan="6">No Staff Loaded for this Department.</td>
			</tr>
		<?php }; ?>
		<tr>
			<td colspan="5" style="text-align: right;"><a href="<?php echo base_url(); ?>staff/new_person" class="btn btn-primary btn-sm">New Staff Member</a></td>
		</tr>
	</table>
</div>
