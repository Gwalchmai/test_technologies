<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Edit Staff Member
 */;
$iStaffID = filter_var($aPerson['id'], FILTER_SANITIZE_NUMBER_INT);

//set up department drop-down
$sDepartmentOptions = '';
foreach($aDepartments as $iKey => $aDepartment) {
	$sDepartmentName = ucfirst($aDepartment['department_name']);
	if($aPerson['department_id'] == $aDepartment['id']) $sDepartmentOptions .= "<option value=\"$aDepartment[id]\" selected>$sDepartmentName</option>"; else $sDepartmentOptions .= "<option value=\"$aDepartment[id]\" >$sDepartmentName</option>";
}
$sSubmitURL = base_url() . "staff/save/$iStaffID";
?>
<div class="container">
	<div class="form-horizontal">
		<?php echo form_open("$sSubmitURL"); ?>
		<fieldset>
			<legend>Edit Staff Member</legend>

			<div class="form-group">
				<label for="inputTitle" class="col-md-2 control-label">Title</label>
				<div class="col-md-10">
					<input class="form-control" name="inputTitle" id="inputTitle" placeholder="Title" value="<?= $aPerson['title'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputFirstname" class="col-md-2 control-label">First Name</label>
				<div class="col-md-10">
					<input class="form-control" name="inputFirstname" id="inputFirstname" placeholder="First Name" value="<?= $aPerson['firstname'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputSurname" class="col-md-2 control-label">Surname</label>
				<div class="col-md-10">
					<input class="form-control" name="inputSurname" id="inputSurname" placeholder="Surname" value="<?= $aPerson['surname'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputStreet" class="col-md-2 control-label">Street Address</label>
				<div class="col-md-10">
					<input class="form-control" name="inputStreet" id="inputStreet" placeholder="Street Address" value="<?= $aPerson['street_address'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputSuburb" class="col-md-2 control-label">Suburb</label>
				<div class="col-md-10">
					<input class="form-control" name="inputSuburb" id="inputSuburb" placeholder="Suburb" value="<?= $aPerson['suburb'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputCity" class="col-md-2 control-label">City</label>
				<div class="col-md-10">
					<input class="form-control" name="inputCity" id="inputCity" placeholder="City" value="<?= $aPerson['city'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputMobile" class="col-md-2 control-label">Mobile Number</label>
				<div class="col-md-10">
					<input class="form-control" name="inputMobile" id="inputMobile" placeholder="Mobile Number" value="<?= $aPerson['mobile_number'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputWork" class="col-md-2 control-label">Work Number</label>
				<div class="col-md-10">
					<input class="form-control" name="inputWork" id="inputWork" placeholder="Work Number" value="<?= $aPerson['work_number'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputEmail" class="col-md-2 control-label">Email Address</label>
				<div class="col-md-10">
					<input class="form-control" name="inputEmail" id="inputEmail" placeholder="Email Address" value="<?= $aPerson['email'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputDepartmentID" class="col-md-2 control-label">Department</label>
				<div class="col-md-10">
					<select class="form-control" name="inputDepartmentID" id="inputDepartmentID"><?= $sDepartmentOptions ?></select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="reset" class="btn btn-default">Cancel</button>
					<input type="submit" name="Submit" id="submit_btn" class="btn btn-primary">
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>