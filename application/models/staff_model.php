<?php
/**
 * Created by: Dylan Moss
 * Date: 30/08/2017
 * Description: Staff Model
 */

class staff_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_staff_list()
	{
		$this->db->join('departments', 'departments.id = staff.department_id');
		$this->db->order_by('firstname, surname', 'ASC');
		$query = $this->db->get_where('staff', array('staff.active' => 1));
		return $query->result_array();
	}

	public function get_unassigned_staff()
	{
		$this->db->order_by('firstname, surname', 'ASC');
		$query = $this->db->get_where('staff', array('department_id' => 0,
		                                             'active' => 1));
		return $query->result_array();
	}

	public function deactivate_person($iStaffID)
	{
		$this->db->query("UPDATE staff SET active = 0 WHERE id = $iStaffID");
	}

	public function get_staff_member($iStaffID)
	{
		$query = $this->db->get_where('staff', array('id' => $iStaffID));
		return $query->row_array();
	}

	public function save_person($iStaffID)
	{
		$sTitle = $this->db->escape_str($this->input->post('inputTitle'));
		$sFirstname = $this->db->escape_str($this->input->post('inputFirstname'));
		$sSurname = $this->db->escape_str($this->input->post('inputSurname'));
		$sStreet = $this->db->escape_str($this->input->post('inputStreet'));
		$sSuburb = $this->db->escape_str($this->input->post('inputSuburb'));
		$sCity = $this->db->escape_str($this->input->post('inputCity'));
		$sMobile = $this->db->escape_str($this->input->post('inputMobile'));
		$sWork = $this->db->escape_str($this->input->post('inputWork'));
		$sEmail = $this->db->escape_str($this->input->post('inputEmail'));
		$iDepartmentID = $this->input->post('inputDepartmentID');

		if($iStaffID > 0) {
			$sSQL = "UPDATE staff SET
				 title = '$sTitle',
				 firstname = '$sFirstname',
				 surname = '$sSurname',
				 street_address = '$sStreet',
				 suburb = '$sSuburb',
				 city = '$sCity',
				 mobile_number = '$sMobile',
				 work_number = '$sWork',
				 email = '$sEmail',
				 department_id = $iDepartmentID
				 WHERE id = $iStaffID";

			$this->db->query("$sSQL");
		}
		else
		{
			$sSQL = "INSERT INTO staff (title, firstname, surname, street_address, suburb, city, mobile_number, work_number, email, department_id) VALUES ('$sTitle', '$sFirstname', '$sSurname', '$sStreet', '$sSuburb', '$sCity', '$sMobile', '$sWork', '$sEmail', $iDepartmentID)";

			$this->db->query($sSQL);
		}
	}
}