<?php
/**
 * Created by: Dylan Moss
 * Date: 30/08/2017
 * Description: Departments controller
 */

Class departments_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_department_list()
	{
		$sSQL = "SELECT d.id AS department_id, d.department_name,
		          s.title, s.firstname, s.surname
		          FROM departments d 
		          JOIN staff s ON d.manager_id = s.id
		          WHERE d.active = 1
		          ORDER BY d.department_name";
		$query = $this->db->query($sSQL);
		/* $this->db->join('staff', 'staff.id = departments.manager_id');
		$this->db->order_by('department_name', 'ASC');
		$query = $this->db->get_where('departments', array('departments.active' => 1)); */
		$aData = $query->result_array();
		foreach($aData as $iKey => $aDepartment) {
			$query = $this->db->get_where('staff', array('department_id' => $aDepartment['department_id'], 'active' => 1));
			$aData[$iKey]['aStaff'] = $query->result_array();
		}
		return ($aData);
	}

	public function deactivate_department($iDepartmentID)
	{
		$this->db->query("UPDATE departments SET active = 0 WHERE id = $iDepartmentID");
		$this->db->query("UPDATE staff SET department_id = 0 WHERE department_id = $iDepartmentID");
	}

	public function get_department_details($iDepartmentID)
	{
		//get the department details
		$sSQL = "SELECT d.id AS department_id, d.department_name, d.manager_id,
		          s.title, s.firstname, s.surname
		          FROM departments d 
		          JOIN staff s ON d.manager_id = s.id
		          WHERE d.active = 1 AND  d.id=$iDepartmentID";
		$query = $this->db->query($sSQL);
		$aData['aDepartment'] = $query->row_array();

		//get the staff in the department
		$query = $this->db->get_where('staff', array('department_id' => $iDepartmentID, 'active' => 1));
		$aData['aStaff'] = $query->result_array();

		return $aData;
	}

	public function save_department($iDepartmentID = 0)
	{
		$sDepartmentName = $this->input->post('inputDepartmentName');
		$iManagerID = $this->input->post('inputManager');
		if($iDepartmentID > 0) {
			$sSQL = "UPDATE departments SET department_name = '$sDepartmentName', manager_id = $iManagerID WHERE id = $iDepartmentID";
			$this->db->query($sSQL);
		}
		else {
			$sSQL = "INSERT INTO departments (department_name, manager_id, active) VALUES ('$sDepartmentName', $iManagerID, 1)";
			$query = $this->db->query($sSQL);
			$sSQL = "SELECT LAST_INSERT_ID()";
			$query = $this->db->query($sSQL);
			$aData = $query->row_array();
			$iID = $aData['LAST_INSERT_ID()'];
			$sSQL = "UPDATE staff SET department_id = $iID WHERE id = $iManagerID";
			$this->db->query($sSQL);
		}
	}

	public function get_unassigned_staff()
	{
		$this->db->order_by('firstname, surname' , 'ASC');
		$query = $this->db->get_where('staff', array('department_id' => 0));
		return $query->result_array();
	}

	public function get_departments(){
		$this->db->order_by('department_name', 'ASC');
		$query = $this->db->get_where('departments', array('active' => 1));
		return $query->result_array();
	}
}